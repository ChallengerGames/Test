package test;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class testFonctionnel {
	DesiredCapabilities capabilities;
	
	private WebDriver driver;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		// On set les tests pour utiliser Geckodriver
		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void testMenu() throws Exception {
		// On se connecte � l'application (L'URL change avec les PC!)
		driver.get("http://10.33.0.149:8100");
		// On assert l'apparition du titre de l'accueil
		assertTrue(driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu-content/ion-nav-bar/div[2]/ion-header-bar/div[1]")).getText().equals("Dashboard"));
		// On selection le bouton du menu et on clique dessus
		driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu-content/ion-nav-bar/div[2]/ion-header-bar/div[2]/span/button")).click();
		Thread.sleep(2000);
		// On clique sur le bouton "jeux"
		driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu/ion-list/div/ion-item[2]")).click();
		Thread.sleep(2000);
		
		driver.navigate().refresh();
		// On v�rifie le titre en tant que "Jeux"
		assertTrue(driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu-content/ion-nav-bar/div[2]/ion-header-bar/div[1]")).getText().equals("Jeux"));
		// On selection le bouton du menu et on clique dessus
		driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu-content/ion-nav-bar/div[2]/ion-header-bar/div[2]/span/button")).click();
		Thread.sleep(500);
		// On clique sur le bouton "Amis"
		driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu/ion-list/div/ion-item[3]")).click();
		Thread.sleep(500);
		
		driver.navigate().refresh();
		// On v�rifie le titre en tant que "Amis"
		assertTrue(driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu-content/ion-nav-bar/div[2]/ion-header-bar/div[1]")).getText().equals("Amis"));
		// On selection le bouton du menu et on clique dessus
		driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu-content/ion-nav-bar/div[2]/ion-header-bar/div[2]/span/button")).click();
		Thread.sleep(500);
		// On clique sur le bouton "Options"
		driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu/ion-list/div/ion-item[4]")).click();
		Thread.sleep(500);
		
		driver.navigate().refresh();
		// On v�rifie le titre en tant que "Options"
		assertTrue(driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu-content/ion-nav-bar/div[2]/ion-header-bar/div[1]")).getText().equals("Options"));
		// On selection le bouton du menu et on clique dessus
		driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu-content/ion-nav-bar/div[2]/ion-header-bar/div[2]/span/button")).click();
		Thread.sleep(500);
		// On clique sur le bouton "Options"
		driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu/ion-list/div/ion-item[1]")).click();
		Thread.sleep(500);
	
		driver.navigate().refresh();
		// On v�rifie le titre en tant que "Dashboard"
		assertTrue(driver.findElement(By.xpath("/html/body/ion-side-menus/ion-side-menu-content/ion-nav-bar/div[2]/ion-header-bar/div[1]")).getText().equals("Dashboard"));
	}
	
	@After
	public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	    	fail(verificationErrorString);
	    }
	}
}